#!/usr/bin/env bash

#TODO PASS5DEV-1168
# - Support hotfixes/patches

#TODO PASS5DEV-1118
# - Automatic version number increase for release and patches.

#VERSION INFO
BITBUCKET_COMMIT_SHORT=${BITBUCKET_COMMIT::7}

VERSION_SUFFIX=""

case $BITBUCKET_BRANCH in
  master)
    VERSION_SUFFIX=""
    ;;

  develop)
    VERSION_SUFFIX="-develop"
    ;;

  *)
    VERSION_SUFFIX="-manual"
    ;;
esac

ASSEMBLY_VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.0.0" 
ASSEMBLY_VERSION_INCLUDING_BUILD="${VERSION_MAJOR}.${VERSION_MINOR}.0.0-${BITBUCKET_BUILD_NUMBER}" 
INFORMATIONAL_VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.0.0-${BITBUCKET_COMMIT_SHORT}${VERSION_SUFFIX}"

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

printf "${HIGHLIGHT_YELLOW}Setting azure artifacts credential provider... \n"
export NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED=$NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
export VSS_NUGET_EXTERNAL_FEED_ENDPOINTS=$VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
wget -O - https://raw.githubusercontent.com/Microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh  | bash

printf "${HIGHLIGHT_YELLOW}Building an testing (also includes building nuget package(s))... \n"
dotnet restore -s $DEVOPS_URL -s "https://api.nuget.org/v3/index.json" $SOLUTION_FILEPATH
dotnet build --no-restore --configuration release $SOLUTION_FILEPATH
dotnet test --filter $DOTNET_TEST_FILTER $SOLUTION_FILEPATH

printf "${HIGHLIGHT_YELLOW}Publishing... \n"
dotnet publish --no-restore -c release -r win-x64 --self-contained false -o 'release-folder' -p:Version=$INFORMATIONAL_VERSION -p:AssemblyVersion=$ASSEMBLY_VERSION $SOLUTION_FILEPATH

printf "${HIGHLIGHT_YELLOW}Tagging commit with the version of the pushed artifact.... \n"
git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/ #because the pipe runs in docker
git tag v${ASSEMBLY_VERSION_INCLUDING_BUILD} ${BITBUCKET_COMMIT}
git push origin --tags

printf "${HIGHLIGHT_YELLOW}Zipping published files... \n"
apt-get install zip -y
cd release-folder
zip -r /usr/share/Installation/build.zip * # add to zip, excludes the root folder, zips subdirectories
cd /usr/share/Installation

# REPLACE PLACEHOLDERS IN POWERSHELL FILES
sed -i 's/PLACEHOLDER_PROJECT_NAME/'$PROJECT_NAME'/g' *.ps1

printf "${HIGHLIGHT_YELLOW}Copying files to aws s3 bucket... \n"
apt-get install awscli -y # install awscli

aws --version #ensuring aws installation
aws configure set aws_access_key_id "$AWS_KEY"
aws configure set aws_secret_access_key "$AWS_SECRET"
aws s3 cp . s3://$AWS_S3_BUCKET_NAME/$PROJECT_NAME/$BITBUCKET_BRANCH/$ASSEMBLY_VERSION_INCLUDING_BUILD/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --recursive 

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code