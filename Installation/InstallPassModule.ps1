﻿function Get-ScriptDirectory { Split-Path $MyInvocation.ScriptName }
$serviceInstallerPath = Join-Path (Get-ScriptDirectory) "ServiceInstaller.ps1"

# Invoking the ServiceInstaller script with parameters
$command = "$serviceInstallerPath -serviceName 'PLACEHOLDER_PROJECT_NAME' -zipFileName 'build.zip' -installationFolder 'C:\Program Files\PLACEHOLDER_PROJECT_NAME' "
Invoke-Expression $command
