﻿When executing InstallPassModule.ps1 and the following error is displayed:
InstallPassModule.ps1 is not digitally signed. You cannot run this script on the current system. For more information about running scripts and setting execution policy, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.

Execute the following lines to unblock the downloaded files.

unblock-file .\InstallPassModule.ps1
unblock-File .\ServiceInstaller.ps1
