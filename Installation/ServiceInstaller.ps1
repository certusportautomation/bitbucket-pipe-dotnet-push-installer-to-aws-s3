﻿param
(
    [string]  $serviceName,
    [string]  $zipFileName,
    [string]  $installationFolder,

    [ValidateSet($null, $true, $false)] [object] $ShouldUninstall = $null,
    [ValidateSet($null, $true, $false)] [object] $ShouldInstall = $null,
    [boolean] $isSilent = $false
)

Add-Type -AssemblyName System.Windows.Forms

# Checking if the script is running as an administrator. 
$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if (!$currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
    Write-Host "You are not running this script as an adminstrator.";
    # ending execution if script is not running as an andminstrator.
    return;
}

######### Helper functions #########

# Gets directory from where the script is running.
function Get-ScriptDirectory { Split-Path $MyInvocation.ScriptName }

function Test-Directory {
    Param (
        [Parameter(Mandatory=$true,Position=0)][string]$Path
    )
 
    try {
        $return = Test-Path -Path $Path -ErrorAction Stop -PathType Container
    } catch [System.UnauthorizedAccessException] {
        return $true
    }
    return $return
}

# Shows a dialog to select a folder and returns the full path;
function Find-Folders {
    [Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
    [System.Windows.Forms.Application]::EnableVisualStyles()
    $browse = New-Object System.Windows.Forms.FolderBrowserDialog
    $browse.SelectedPath = "C:\"
    $browse.ShowNewFolderButton = $false
    $browse.Description = "Select a directory"

    $loop = $true
    while($loop)
    {
        if ($browse.ShowDialog() -eq "OK")
        {
        $loop = $false
        } 
        else
        {
            $res = [System.Windows.Forms.MessageBox]::Show("You clicked Cancel. Would you like to try again or exit?", "Select a location", [System.Windows.Forms.MessageBoxButtons]::RetryCancel)
            if($res -eq "Cancel")
            {
                #Ends script
                return
            }
        }
    }
    $returnPath = $browse.SelectedPath
    
    $browse.Dispose()

    return $returnPath;
} 

######### Installation procedure #########
$zipFilePath = Join-Path (Get-ScriptDirectory) $zipFileName
$installationFolderExists = Test-Directory($installationFolder);
# checking if the user only wants to uninstall the service

if($isSilent)
{
    #Clean install
    $ShouldUninstall = $true;
    $ShouldInstall = $true;
}
Else #User input required
{
    if($installationFolderExists -and ($ShouldUninstall -eq $null))
    {
        $ShouldUninstallDialogResult =  [System.Windows.Forms.MessageBox]::Show("Uninstall $serviceName ?",$serviceName ,'YesNo','Warning', 'Button1')
        if ($ShouldUninstallDialogResult -eq 'Yes') 
        {
            $ShouldUninstall = $true;
        }
    }


    if($ShouldInstall -eq $null)
    {
        $ShouldInstallDialogResult =  [System.Windows.Forms.MessageBox]::Show("Install $serviceName ?",$serviceName ,'YesNo','Question', 'Button1')
        if ($ShouldInstallDialogResult -eq 'Yes') {
            $ShouldInstall = $true;

             # give the user the option to overwrite the installation folder.
            $overWriteInstallationFolder =  [System.Windows.Forms.MessageBox]::Show("Do you want to overwrite the standard installation directory '$installationFolder' ?", $ServiceName, 'YesNo','Question', 'Button2')
            if ($overWriteInstallationFolder -eq 'Yes') 
            {
                $installationFolder = Find-Folders
            }
        }
    }
}

#Uninstall script
if($ShouldUninstall -and $installationFolderExists)
{
    Write-Host Uninstalling $serviceName...;
    $executables = Get-Childitem –Path $installationFolder -Filter *.exe -File -ErrorAction SilentlyContinue;
    foreach($executable in $executables){
        Invoke-Expression ".'$(Join-Path $installationFolder $executable)' uninstall" | Out-Null;
    }

    #Removing the installation folder.
    Remove-Item –path $installationFolder -Recurse -Force
    Write-Host Successfully uninstalled $serviceName -ForegroundColor Yellow
}

#install script
if($ShouldInstall)
{
    Write-Host Installing $serviceName...;

    Expand-Archive -LiteralPath $zipFilePath -DestinationPath $installationFolder -Force

    # Run the install and start command for all executables
    $installExecutables = Get-Childitem –Path $installationFolder -Filter *.exe -File -ErrorAction SilentlyContinue;
    foreach($installExecutable in $installExecutables){
        #Write-Host "Installing $installExecutable ...";
        Invoke-Expression ".'$(Join-Path $installationFolder $installExecutable)' install"  | Out-Null;
        #Write-Host "Starting $installExecutable ...";
        Invoke-Expression ".'$(Join-Path $installationFolder $installExecutable)' start"  | Out-Null;
    }

    Write-Host Successfully installed $serviceName -ForegroundColor Green
}